# %%
import numpy as np
import matplotlib.pyplot as plt
from ximea import xiapi
import HWS
from HWS.HS_Centroids import HS_Centroids
from HWS.HS_Image import HS_Image
from HWS.HS_Gradients import HS_Gradients

# %%%
# Set up communication with ximea camera:
cam = xiapi.Camera()
cam.open_device()

# Camera settings
cam.set_exposure(2000) #2000 micro sec
print('Exposure was set to %i us' %cam.get_exposure())
cam.set_imgdataformat('XI_MONO16') # Set data to be stored in 16 bit fomrat
print('Image data format:' +cam.get_imgdataformat())
cam.set_output_bit_depth('XI_BPP_12') # Set output bit depth to 12 bit (maximum)
print('Output bit depth:' +cam.get_output_bit_depth())
cam.set_image_data_bit_depth('XI_BPP_12') # Set image bit depth to 12 bit (maximum)
print('Image data bit depth :' +cam.get_image_data_bit_depth())

# %%
# Set path to save images:
img_dir = 'C:/Users/htuon/OneDrive/Documents/HWS/test/'

# %% 
# Create an instance of XimeaImage to store reference image data
img = xiapi.Image()
imgs_ref = []
# Start data acquisition
print('Starting data acquisition...')
cam.start_acquisition()

Nframes = 50 # Number of image frames

for i in range(Nframes):
    #get data and pass them from camera to img
    cam.get_image(img)
    data= img.get_image_data_numpy()
    # Optional save image to folder 
    np.savez(img_dir+'ref_%04d'%i,data=data)
    imgs_ref.append(data)

print('Stopping acquisition...')
cam.stop_acquisition()

# %%
# Process images
imgs_ref = np.asarray(imgs_ref)
img_ref_avg = np.mean(imgs_ref, axis=0)

hsi_ref = HS_Image()
hsi_ref.background = 100
hsi_ref.original_image = img_ref_avg
hsi_ref.process_image()

# %%
# Find reference centroids:
hsc_ref = HS_Centroids()
hsc_ref.hsimage = hsi_ref
hsc_ref.radius = 20
hsc_ref.find_centroids_from_image()

# %%
#Take live images:
imgs_live = []
# Start data acquisition
cam.start_acquisition()

for i in range(Nframes):
    #get data and pass them from camera to img
    cam.get_image(img)
    data = img.get_image_data_numpy()
    # Optional save image to folder 
    np.savez(img_dir+'live_%04d'%i,data=data)
    imgs_live.append(data)

print('Stopping acquisition...')
cam.stop_acquisition()

# %%
# Process live image
imgs_live = np.asarray(imgs_live)
img_live_avg = np.mean(imgs_live, axis=0)

hsi_live = HS_Image()
hsi_live.background = 100
hsi_live.original_image = img_live_avg
hsi_live.process_image()

# %%
# Find live centroids
hsc_live = HS_Centroids()
hsc_live.hsimage = hsi_live
hsc_live.radius = 20
hsc_live.find_centroids_using_tempate(hsi_ref.centroids)

# %%
hsg = HS_Gradients(hsc_ref, hsc_live)
hsg.origin =  np.array([1023.5,1023.5]) # Thes ensor size is 2048 x 2046
hsg.lever_arm = 8.03e-3 # Previously calibrated assembly level arm length
hsg.pixel_size = 5.5e-6 # Ximea pixel size
hsg.construct_gradients()